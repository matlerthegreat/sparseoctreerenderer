﻿# Real-time sparse octree renderer

This program uses space efficient data structure to hold geometry and fast algorithm to trace light rays through it. It can even be run on a powerfull enough smartphone.

# Results

![Schwarzschild black hole](media/forest.png "Sparse octree")
![Schwarzschild black hole](media/shadows.png "Sparse octree")
![Schwarzschild black hole](media/android.jpg "Sparse octree")
