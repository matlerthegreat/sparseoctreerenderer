#include "GGVoxels.h"
#include <iostream>
#include <random>

struct GGSphere {
    vec3 pos;
    float radius;
    uint element;
};

inline float sqr(float x)
{
    return x * x;
}

vec2 get_min_max_distance_squared(vec3 cube_min, vec3 cube_max, vec3 sphere)
{
    float max_distance = 0.0f;
    float min_distance = 0.0f;

    if (sphere.x < cube_min.x)
        min_distance += sqr(sphere.x - cube_min.x);
    else if (sphere.x > cube_max.x)
        min_distance += sqr(sphere.x - cube_max.x);
    max_distance += max(sqr(sphere.x - cube_min.x), sqr(sphere.x - cube_max.x));

    if (sphere.y < cube_min.y)
        min_distance += sqr(sphere.y - cube_min.y);
    else if (sphere.y > cube_max.y)
        min_distance += sqr(sphere.y - cube_max.y);
    max_distance += max(sqr(sphere.y - cube_min.y), sqr(sphere.y - cube_max.y));

    if (sphere.z < cube_min.z)
        min_distance += sqr(sphere.z - cube_min.z);
    else if (sphere.z > cube_max.z)
        min_distance += sqr(sphere.z - cube_max.z);
    max_distance += max(sqr(sphere.z - cube_min.z), sqr(sphere.z - cube_max.z));

    return vec2{min_distance, max_distance};
}

void populate_voxels(uint index, uint parent, uint depth, uint max_depth, float radius, vec3 pos, const std::vector<GGSphere>& spheres, GGVoxels& voxels)
{
    voxels[index].parent = parent;
	bool intersects_sphere = false;
	for (const auto& sphere : spheres) {
    	auto cube_min = pos - vec3(radius);
    	auto cube_max = pos + vec3(radius);
    	auto distance = get_min_max_distance_squared(cube_min, cube_max, sphere.pos);
		if (distance[1] <= sqr(sphere.radius)) {
			voxels[index].data = sphere.element << 1;
			break;
		}
    	else if (distance[0] <= sqr(sphere.radius)) {
    		if (depth == max_depth) {
    			voxels[index].data = sphere.element << 1;
    		}
    		else {
    			voxels[index].data = 1;
    			voxels[index].children = voxels.size();
    			++depth;
    			radius /= 2.0f;
    			parent = index;
    			index = voxels.size();
    			pos -= vec3(radius);
    			for (uint i = 0; i < 8; ++i)
        			voxels.emplace_back();

    			for (uint x = 0; x < 2; ++x) {
        			for (uint y = 0; y < 2; ++y) {
            			for (uint z = 0; z < 2; ++z) {
                			auto dpos = vec3(x * radius * 2.0f, y * radius * 2.0f, z * radius * 2.0f);
                			populate_voxels(index, parent, depth, max_depth, radius, pos + dpos, spheres, voxels);
                			++index;
            			}
        			}
    			}
    		}
			break;
		}
	}
}

void generate_tree(std::vector<GGSphere>& spheres, vec3 pos, float scale)
{
    for (auto i = 0; i < 8; ++i) {
        auto offset = vec3(0.0f, i * 0.1f * scale, 0.0f);
        spheres.push_back({pos + offset, 0.2f * scale, 2});
    }
	auto offset = vec3(0.0f, 1.6f * scale, 0.0f);
	spheres.push_back({pos + offset, 0.8f * scale, 3});
}

GGVoxels generate_voxels()
{
    GGVoxels voxels(2);
    const uint max_depth = 6;

	std::vector<GGSphere> spheres;
	spheres.push_back({{-0.55f, 0.02f, -0.55f}, 0.03f, 7});

	/*spheres.push_back({{0.1f, 0.2f, 0.5f}, 0.3f, 1});
	spheres.push_back({{-0.2f, -0.2f, 0.0f}, 0.5f, 2});
	spheres.push_back({{0.0f, -100.4f, 0.0f}, 100.0f, 3});
	spheres.push_back({{0.0f, 0.0f, 100.8f}, 100.0f, 4});
	spheres.push_back({{100.8f, 0.0f, 0.0f}, 100.0f, 4});*/

	spheres.push_back({{0.6f, 0.0f, 0.025f}, 0.05f, 0});
	spheres.push_back({{0.6f, -0.025f, 0.05f}, 0.05f, 0});
	spheres.push_back({{0.6f, -0.05f, 0.075f}, 0.05f, 0});
	spheres.push_back({{0.6f, -0.1f, 0.2f}, 0.15f, 0});

    spheres.push_back({{0.6f, -0.15f, 0.4f}, 0.1f, 7});

	spheres.push_back({{0.6f, -0.15f, 0.4f}, 0.4f, 5});
	spheres.push_back({{0.6f, -0.33f, 0.2f}, 0.4f, 5});
	spheres.push_back({{0.6f, -0.3f, 0.4f}, 0.5f, 1});

	spheres.push_back({{0, -100.05f, 0}, 100.0f, 4});
	spheres.push_back({{-0.06f, -0.2f, 0.03f}, 0.3f, 0});
	spheres.push_back({{-0.12f, -0.2f, 0.1f}, 0.3f, 0});
	spheres.push_back({{0.0f, -0.2f, 0.0f}, 0.3f, 0});
	spheres.push_back({{0, -100.0f, 0}, 100.0f, 1});

	/*generate_tree(spheres, vec3(-0.44f, -0.0f, -0.55f), 0.12f); 
	generate_tree(spheres, vec3(-0.39f, -0.0f, -0.35f), 0.11f); 
	generate_tree(spheres, vec3(-0.3f, -0.0f, -0.5f), 0.13f); 
	generate_tree(spheres, vec3(-0.15f, -0.0f, -0.4f), 0.08f); 
	generate_tree(spheres, vec3(0.1f, -0.0f, -0.6f), 0.16f); 
	generate_tree(spheres, vec3(0.25f, -0.0f, 0.15f), 0.07f); 
	generate_tree(spheres, vec3(0.35f, -0.0f, -0.25f), 0.1f); */



    std::default_random_engine gen;
    std::uniform_real_distribution<float> dist(-0.4f,0.4f);
    for (int i = 0; i < 24; ++i) {
        generate_tree(spheres, vec3(-0.5f + dist(gen), -0.0f, -0.5f + dist(gen)), 0.15f + dist(gen)/10.0f); 
    }


	generate_tree(spheres, vec3(-0.5f, -0.0f, 0.55f), 0.4f); 

	populate_voxels(0, 0, 0, max_depth, 1.0f, vec3(0), spheres, voxels);

	spheres.clear();
	spheres.push_back({{0.0f, 0.0f, 0.0f}, 1.0f, 6});
	populate_voxels(1, 0, 0, 4, 1.0f, vec3(0), spheres, voxels);

	std::cout << voxels.size() << std::endl;
	return voxels;
}
