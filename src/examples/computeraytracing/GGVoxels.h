#pragma once

#include <vector>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
using namespace glm;

struct GGVoxel
{
    uint data = 0;
    uint children = 0;
    uint parent = 0;
    uint padding;
};

using GGVoxels = std::vector<GGVoxel>;

GGVoxels generate_voxels();
